# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelSQL, ModelView


class ContactMechanism(ModelSQL, ModelView):
    _name = 'party.contact_mechanism'

    def vcard2values(self, cm, cm_type):
        vals = super(ContactMechanism, self).vcard2values(cm, cm_type)
        vals['intended_use'] = False
        if hasattr(cm, 'type_paramlist'):
            paramlist = [x.lower() for x in cm.type_paramlist]
            if cm_type in ('phone', 'email', 'website', 'fax'):
                if 'home' in paramlist:
                    vals['intended_use'] = 'home'
                elif 'work' in paramlist:
                    vals['intended_use'] = 'work'
        return vals

    def _cm2type_paramlist(self, cm, vcard_cm):
        res = super(ContactMechanism, self)._cm2type_paramlist(cm, vcard_cm)
        if (cm.type in ('email', 'phone', 'website', 'fax') and
                cm.intended_use not in ['other', False, None]):
            if cm.intended_use not in res:
                res.append(cm.intended_use)
        return res

ContactMechanism()

